																							HW1 CSC 548 Parallel systems

Group Information
* sananth5 Srinath Krishna Ananthakrishnan
* sramach6 Subramanian Ramachandran
* vchandr3 Vinoth Kumar Chandra Mohan

==========================================================================

DESCRIPTION:
Parallelized version of Derivative computation via Finite difference method and Integral computation via Trapezoidal rule.
Code in "hw1_mpi.c" consists of different functions for:-
1. Blocking Communication 
2. Non-blocking communication
3. MPI_Reduce and
4. Manual P2P Tree Reduction

The logic and ideas used are described in comments inside the code.

COMPILATION:
Run make
Executable "hw1_mpi" should be generated
$ make


EXECUTION:
Create a qsub file pointing to the executable's path  "hw1_mpi"
$ qsub  xxx.qsub

OUTPUT:
1. err.dat is generated in the home directory.
	It lists the avg error, standard deviaton , integral error and the derivative errors at each grid point
2. hw1_mpi.out is generated in the current directory.
	It provides the timing results of different methods (blocking/non-blocking , MPI_Reduce/P2P tree reduction, Error Gather)



ANALYSIS:

Our tests were with the following functions
- sqrt(x)
- x^2
- sin(x)
- e^x
- sinh(x)

Problem only scaling
--------------------
For tests with a higher number of points (100 v 65536) and the same number of
processors (10) (problem only scaling), the time taken for the differential
and the integral computation was significantly large when the grid points were higher.
This was because each task now has to compute a larger number of points.

For the blocking and non-blocking communication, we could see that the
non-blocking versions were better performant because communication and computation
were interleaved. For the test, communication + computation took lesser
time for the version with lesser number of points. This is understandable
as the computation at each node is linear to the problem size.

We also consistently noticed that a manual tree based reduction was slightly better
than the naive MPI_Reduce. This could be because of the fact that MPI_Reduce might
have to endure some overhead in determining the layout in which a Reduce should
happen, while the tree based reduction uses a simple logic to perform reduction.
Moreover the communication and computation is concentrated at Task 0 which increases the
load in Task 0. While in Tree based reduce the communication and computation are distributed 
almost evenly across all participating Tasks.

Here are the results for problem only scaling.

Timing results; 100 grid points, 10 nodes
Finite difference blocking: 4.432797e-02 s
Finite difference non-blocking: 1.978874e-05 s
Integration MPI_REDUCE reduction: 8.982182e-03 s
Integration P2P tree reduction: 5.557060e-03 s
Error gather: 5.805016e-03 s

Timing results; 65536 grid points, 10 nodes
Finite difference blocking: 3.929305e-01 s
Finite difference non-blocking: 8.239746e-04 s
Integration MPI_REDUCE reduction: 7.231951e-03 s
Integration P2P tree reduction: 4.223108e-03 s
Error gather: 3.081083e-02 s

Processor only scaling
----------------------
For processor only scaling, we could notice that the communication + computation
time was slightly lesser for the case with the larger number of processors. This
is because, even though there are a large number of processes, each process
communicates only with its neighbours and this happens in parallel. The bottleneck
becomes the computation which decreases with an increase in number of processes
because of the fact that each process now works on a lesser number of points.

Also, for a larger number of processors, MPI_Reduce takes more time. This is
expected as the reduce needs to propagate through more levels (assuming a tree
layout).

Here are the results for processor only scaling.

Timing results; 100 grid points, 10 nodes
Finite difference blocking: 4.432797e-02 s
Finite difference non-blocking: 1.978874e-05 s
Integration MPI_REDUCE reduction: 8.982182e-03 s
Integration P2P tree reduction: 5.557060e-03 s
Error gather: 5.805016e-03 s

Timing results; 100 grid points, 50 nodes
Finite difference blocking: 3.046918e-02 s
Finite difference non-blocking: 1.788139e-05 s
Integration MPI_REDUCE reduction: 4.541206e-02 s
Integration P2P tree reduction: 1.905608e-02 s
Error gather: 1.788139e-05 s

One surprising observation was the with a higher number of nodes, MPI_Gather
was faster than for the case with a lesser number of nodes. Intuitively, with
a larger number of nodes, the root might have to collect a large number of vectors
and hence will take longer time than the test with a lesser number of nodes.
However, with a larger number of nodes, we could notice that MPI_Gather actually
took lesser time. This could be due to the implementation, wherein with a larger
number of nodes, a more efficient layout could be chosen.



Problem and Processor scaling
-----------------------------
When scaling both the problem and the number of processors, we noticed that time taken to compute 
the derivate computation+communication was of more or less the same order.

For integration: As expected MPI_Reduce performance was considerably slower with increase in nodes
while P2P tree reduction continued to perfom better.

Timing results; 100 grid points, 10 nodes
Finite difference blocking: 4.432797e-02 s
Finite difference non-blocking: 1.978874e-05 s
Integration MPI_REDUCE reduction: 8.982182e-03 s
Integration P2P tree reduction: 5.557060e-03 s
Error gather: 5.805016e-03 s

Timing results; 65536 grid points, 64 nodes
Finite difference blocking: 3.551078e-02 s
Finite difference non-blocking: 9.894371e-05 s
Integration MPI_REDUCE reduction: 5.339003e-02 s
Integration P2P tree reduction: 3.089905e-04 s
Error gather: 4.678521e-01 s

Errors and the grid points
--------------------------
We noticed that with a larger number of grid points and the same XI and XF ranges,
the error reduces. This is because, There are more points in the range (better
sampling) and hence the accuracy in computing the derivative/integral increases
(because dx decreases). As dx decreases further, the computed value gets closer
to the actual value as per the definition's limit dx->0.

Errors and functions
--------------------
Errors are also related to the function that is plotted.
If the slope of the curve is high in most part of the curve, then
the avg error will be higher.

