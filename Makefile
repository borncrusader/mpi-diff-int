all: hw1 hw1_mpi

hw1_mpi: hw1_mpi.c
	mpicc -g hw1_mpi.c -o hw1_mpi -lm

hw1: hw1.c
	gcc -g -o hw1 hw1.c -lm

qsub: hw1_mpi.qsub
	qsub hw1_mpi.qsub

out:
	rm -rf hw1_mpi.e* hw1_mpi.out

clean: out
	rm -rf hw1 hw1_mpi
