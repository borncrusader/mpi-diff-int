/******************************************************************************
* FILE: hw1_mpi.c
* DESCRIPTION:
* 
* This file provides a parallel computation of a function's Derivative
* (using Finite Difference method) and Integrals (using Trapezoidal rule)
* 
* Users will supply the functions
* i.) fn(x) - the function to be analyized
* ii.) dfn(x) - the true derivative of the function
* iii.) ifn(x) - the true integral of the function
*
* The function fn(x) should be smooth and continuous, and
* the derivative and integral should have analyitic expressions
* on the entire domain.
*  
* Users will also need to specify XI , XF and NGRID
*
* Group info:
*
* sananth5 Srinath Krishna Ananthakrishnan (skeleton, blocking)
* sramach6 Subramanian Ramachandran (skeleton, tree-based reduce)
* vchandr3 Vinoth Kumar Chandra Mohan (skeleton, non-blocking)
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mpi.h>

#define   NGRID           65536     /* number of grid points */
#define   XI              1.0       /* first grid point */
#define   XF              100.0     /* last grid point */

#define eprintf(rank, fmt, ...) \
  fprintf(stderr, "%d: " fmt, rank, ##__VA_ARGS__)

typedef   double   FP_PREC;

/* some global vars */
int rank, num_tasks, ngrids_per_task;
/* offset gives the exact starting xc[i] for each task*/
int offset;
/* difference between xc[i]- xc[i-1]*/
FP_PREC dx;

/* returns the function y(x) = fn */
FP_PREC fn(FP_PREC x)
{
  return sqrt(x);
  //return pow(x, 2);
  //return sin(x);
  //return pow(M_E, x);
  //return (x*x);
  //return (sinh(x));
}

/* returns the derivative d(fn)/dx = dy/dx */
FP_PREC dfn(FP_PREC x)
{
  return 0.5*(1.0/sqrt(x));
  //return 2*x;
  //return cos(x);
  //return pow(M_E, x);
  //return (2*x);
  //return (cosh(x));
}

/* returns the integral from a to b of y(x) = fn */
FP_PREC ifn(FP_PREC a, FP_PREC b)
{
  return (2.0/3.0) * (pow(sqrt(b), 3) - pow(sqrt(a),3));
  //return (pow(b,3)/3.0) - (pow(a,3)/3.);
  //return -cos(b) + cos(a);
  //return pow(M_E, b) - pow(M_E, a);
  //return (1.0/3.0)*(pow(b,3) - pow(a,3));
  //return (cosh(b) - cosh(a));
}

/* allocate memory for xc, yc and dyc */
int alloc_memory(FP_PREC **xc, FP_PREC **yc, FP_PREC **dyc, FP_PREC **derr)
{
  int num_elem;

  /* for rank 0, we should allocate an array with NGRID+2 points as we need
   * to gather all the data from other nodes
   * for all other nodes, array should be allocated for the number of grid
   * points per task+2
   */
  if (rank == 0) {
    num_elem = NGRID + 2;
  } else {
    num_elem = ngrids_per_task + 2;
  }

  *xc = (FP_PREC*) malloc((num_elem + 2) * sizeof(FP_PREC));
  *yc = (FP_PREC*) malloc((num_elem + 2) * sizeof(FP_PREC));
  *dyc = (FP_PREC*) malloc((num_elem + 2) * sizeof(FP_PREC));
  *derr = (FP_PREC*) malloc((num_elem + 2) * sizeof(FP_PREC));

  if (!xc || !yc || !dyc || !derr) {
    return 1;
  }

  return 0;
}

/* construct grid xc and define yc and points from imin to imax */
int construct_grid(int imin, int imax,
                   FP_PREC *xc, FP_PREC *yc)
{
  int i;

  if (rank == 0) {
    imax = NGRID;
  }

  for (i = imin; i <= imax; i++) {
    xc[i] = XI + (FP_PREC)((i+offset)- 1) * dx;
  }

  xc[0] = xc[1] - dx;
  xc[imax + 1] = xc[imax] + dx;

  return 0;
}

/* exchange boundary data through blocking p-p communication
 * and compute dyc and intg
 */
int xchg_and_compute_blocking(int imin, int imax, FP_PREC *xc,
                              FP_PREC *yc, FP_PREC *dyc, FP_PREC *intg)
{
  MPI_Status stat;
  int i;

  for (i = imin; i <= imax; i++) {
    yc[i] = fn(xc[i]);
  }

  /* set boundary values, all the other values will be exchanged */
  if (rank == 0) {
    yc[0] = fn(xc[0]);
  }
  if (rank == num_tasks - 1) {
    yc[imax + 1] = fn(xc[imax + 1]);
  }

  /* if the node has an even rank, it first waits for receive and then sends
   * if the node has an odd rank, it first sends and then receives
   * this is done so as to prevent deadlock and improve efficiency in data transfer
   */
  if ((rank & 0x1) == 0) {
    if (rank != 0) {
      MPI_Recv(&yc[0], 1, MPI_DOUBLE, rank-1, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
      MPI_Send(&yc[1], 1, MPI_DOUBLE, rank-1, 1, MPI_COMM_WORLD);
    }
    if (rank != num_tasks - 1) {
      MPI_Recv(&yc[imax+1], 1, MPI_DOUBLE, rank+1, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
      MPI_Send(&yc[imax], 1, MPI_DOUBLE, rank+1, 2, MPI_COMM_WORLD);
    }
  } else {
    if (rank != num_tasks-1) {
      MPI_Send(&yc[imax], 1, MPI_DOUBLE, rank+1, 2, MPI_COMM_WORLD);
      MPI_Recv(&yc[imax+1], 1, MPI_DOUBLE, rank+1, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
    }
    MPI_Send(&yc[1], 1, MPI_DOUBLE, rank-1, 1, MPI_COMM_WORLD);
    MPI_Recv(&yc[0], 1, MPI_DOUBLE, rank-1, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
  }

  /* compute the derivative */
  for (i = imin; i <= imax; i++) {
    dyc[i] = (yc[i + 1] - yc[i - 1])/(2.0 * dx);
  }

  *intg = 0.0;
  for (i = imin; i <= imax; i++) {
    /* there are NGRID points, so there are NGRID-1 integration zones. */
    if ((i+offset) - imin != NGRID - 1) {
      *intg += 0.5 * (xc[i + 1] - xc[i]) * (yc[i + 1] + yc[i]);
    }
  }

  return 0;
}

/* exchange boundary data through non-blocking p-p communication
 * and compute dyc and intg
 */
int xchg_and_compute_non_blocking(int imin, int imax, FP_PREC *xc,
                                  FP_PREC *yc, FP_PREC *dyc, FP_PREC *intg)
{
  MPI_Request reqs[4];
  MPI_Status stats[4];
  int i;

  /* Compute the values which are needed by other neighbour processes
   * first, then compute the remaining after non-blocking send/recv
   */
  yc[imin] = fn(xc[imin]);
  yc[imax] = fn(xc[imax]);

  if ((rank & 0x1) == 0) {
    if (rank != 0) {
      MPI_Irecv(&yc[0], 1, MPI_DOUBLE, rank - 1, MPI_ANY_TAG, MPI_COMM_WORLD, &reqs[2]);
      MPI_Isend(&yc[imin], 1, MPI_DOUBLE, rank - 1, 1, MPI_COMM_WORLD, &reqs[3]);
    }
    if (rank != num_tasks - 1) {
      MPI_Irecv(&yc[imax + 1], 1, MPI_DOUBLE, rank + 1, MPI_ANY_TAG, MPI_COMM_WORLD, &reqs[0]);
      MPI_Isend(&yc[imax], 1, MPI_DOUBLE, rank + 1, 2, MPI_COMM_WORLD, &reqs[1]);
    }
  } else {
    if (rank != num_tasks - 1) {
      MPI_Isend(&yc[imax], 1, MPI_DOUBLE, rank + 1, 2, MPI_COMM_WORLD, &reqs[0]);
      MPI_Irecv(&yc[imax + 1], 1, MPI_DOUBLE, rank + 1, MPI_ANY_TAG, MPI_COMM_WORLD, &reqs[1]);
    }
    MPI_Isend(&yc[imin], 1, MPI_DOUBLE, rank - 1, 1, MPI_COMM_WORLD, &reqs[2]);
    MPI_Irecv(&yc[0], 1, MPI_DOUBLE, rank - 1, MPI_ANY_TAG, MPI_COMM_WORLD, &reqs[3]);
  }

  /* define the function */
  for (i = imin + 1; i <= imax - 1; i++ ) {
    yc[i] = fn(xc[i]);
  }

  /* set boundary values */
  if (rank == 0) {
    yc[0] = fn(xc[0]);
  }
  if (rank == num_tasks - 1) {
    yc[imax + 1] = fn(xc[imax + 1]);
  }

  /* compute the derivative */
  for (i = imin + 1; i <= imax - 1; i++) {
    dyc[i] = (yc[i + 1] - yc[i - 1])/(2.0 * dx);
  }

  /* compute the integral */
  *intg = 0.0;
  for (i = imin; i <= imax - 1; i++) {
    *intg += 0.5 * (xc[i + 1] - xc[i]) * (yc[i + 1] + yc[i]);
  }

  /* wait for all the reqs to complete */
  if (rank == 0) {
    MPI_Waitall(2, reqs, stats);
  } else if ( rank == num_tasks -1) {
    MPI_Waitall(2, &reqs[2], stats);
  } else {
    MPI_Waitall(4, reqs, stats);
  }

  /* compute the boundary derivatives and integral */
  dyc[imin] = (yc[imin + 1] - yc[imin - 1])/(2.0 * dx);
  dyc[imax] = (yc[imax + 1] - yc[imax - 1])/(2.0 * dx);

  if ( (imax + offset) - imin != NGRID - 1) {
    *intg += 0.5 * (xc[imax + 1] - xc[imax]) * (yc[imax + 1] + yc[imax]);
  }

  return 0;
}

/* compute the error vectors, derr */
int compute_errors(int imin, int imax, FP_PREC *xc, FP_PREC *dyc, FP_PREC *derr)
{
  int i;

  for(i = imin; i <= imax; i++) {
    derr[i] = fabs((dyc[i] - dfn(xc[i]))/dfn(xc[i]));
  }

  return 0;
}

/* gather all the derrs from all the processes */
int gather_from_all_procs(FP_PREC *bin)
{
  int residue;

  MPI_Status stat;

  /* send only NGRID / num_tasks, extra grids from the last node
   * will be received later
   */
  MPI_Gather(&bin[1], NGRID / num_tasks, MPI_DOUBLE, &bin[1], ngrids_per_task, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  /* transact residue from the last task */
  residue = NGRID % num_tasks;
  if (residue) {
    if (rank == 0) {
      MPI_Recv(&bin[num_tasks*ngrids_per_task + 1], residue, MPI_DOUBLE, num_tasks-1, 1, MPI_COMM_WORLD, &stat);
    } else if (rank == num_tasks - 1) {
      MPI_Send(&bin[NGRID/num_tasks + 1], residue, MPI_DOUBLE, 0, 1, MPI_COMM_WORLD);
    }
  }

  return 0;
}

/** manual P2P tree reduction
 * This function implements a binary tree reduction where the size reduces by half every step
 * The final integral value is available at Task 0
 */
int manual_tree_based_reduce(FP_PREC *intg)
{
  MPI_Status stat;
	/*len of tree initialized to number of tasks*/
  int i,len=num_tasks;
  FP_PREC tmp_intg;
  int peer_task=0;
	/*A task participates in P2P reduction as long as its rank is less than current len*/
  while (len > 0 && rank < len) {
    if ( rank >= len/2) {
			/*Find the corresponding peer task and send my integral value
			 *All residues (if the length is not even )are sent to task 0
			 */
      peer_task = rank < len/2*2?rank-len/2:0; /*Uses integer division :) */
      MPI_Send(intg, 1, MPI_DOUBLE, peer_task, 0, MPI_COMM_WORLD);
    } else {
			/*Find the corresponding peer task and receive it's integral value
			 *Update the partial integral in the current task
			 */
      peer_task = rank +len/2;
      MPI_Recv(&tmp_intg, 1, MPI_DOUBLE, peer_task, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
      *intg += tmp_intg;
      if (rank == 0) {
				/*Receive any residual values at task 0*/
        for (i=len/2*2; i < len; ++i) {
          MPI_Recv(&tmp_intg, 1, MPI_DOUBLE, i, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
          *intg += tmp_intg;
        }
      }
    }
		/*Length of tree reduces by half for each step (Binary tree)*/
    len = len/2;
  }

  return 0;
}

/* This function computes the Average error in derivatives, The Standard Deviation and Integral errors*/
int compute_std_dev(FP_PREC *derr, FP_PREC intg, FP_PREC *davg_err,
                    FP_PREC *dstd_dev, FP_PREC *intg_err)
{
  int i;
  
  *davg_err = 0.0;
  for(i = 1; i <= NGRID ; i++) {
    *davg_err += derr[i];
  }
    
  *davg_err /= (FP_PREC)NGRID;

  *dstd_dev = 0.0;
  for(i = 1; i <= NGRID; i++) {
    *dstd_dev += pow(derr[i] - *davg_err, 2);
  }
  *dstd_dev = sqrt(*dstd_dev/(FP_PREC)NGRID);
  
  *intg_err = fabs((ifn(XI, XF) - intg)/ifn(XI, XF));
 
  return 0;
}

void print_error_data(FP_PREC avgerr, FP_PREC stdd, FP_PREC *x, FP_PREC *err, FP_PREC ierr)
{
  int   i;
 
  FILE *fp  = fopen("err.dat", "w");

  fprintf(fp, "%e\n%e\n%e\n", avgerr, stdd, ierr);
  for(i = 0; i < NGRID; i++) {
    fprintf(fp, "%e %e \n", x[i], err[i]);
  }

  fclose(fp);
}

int main(int argc, char *argv[])
{
  /* range between 1 and ngrids_per_task */
  int imin, imax;
  /* offset + index will give the true position */
  int i;
  double start,end;

  FP_PREC intg, fintg;
  FP_PREC *xc, *yc, *dyc;
  FP_PREC *derr;
  FP_PREC davg_err, dstd_dev, intg_err;

  MPI_Init(&argc, &argv);

  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &num_tasks);

	/*Precompute dx*/
  dx = ((FP_PREC)(XF-XI))/((FP_PREC) (NGRID-1));
  ngrids_per_task = NGRID / num_tasks;
  offset = ngrids_per_task * rank;
  if (rank == num_tasks - 1) {
    /* last task should take up the remaining grid points */
    ngrids_per_task = NGRID - (num_tasks - 1) * ngrids_per_task;
  }

  imin = 1;
  imax = ngrids_per_task; 

  /* allocate memory for xc, yc and dyc */
  if (alloc_memory(&xc, &yc, &dyc, &derr)) {
    eprintf(rank, "cannot allocate memory!\n");
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  /* construct the grid */
  construct_grid(imin, imax, xc, yc);

  /* exchange boundary data and compute dyc and intg (blocking) */
  if (rank ==0) {
    printf("Timing results; %d grid points, %d nodes\n", NGRID, num_tasks);
	  start = MPI_Wtime();
  }
  xchg_and_compute_blocking(imin, imax, xc, yc, dyc, &intg);
  if (rank ==0) {
  	end = MPI_Wtime();
	  printf("Finite difference blocking: %e s\n", end - start);
  }

  /* exchange boundary data and compute dyc and intg (non-blocking) */
  if (rank ==0) {
	  start = MPI_Wtime();
  }
  xchg_and_compute_non_blocking(imin, imax, xc, yc, dyc, &intg);
  if (rank ==0) {
  	end = MPI_Wtime();
	  printf("Finite difference non-blocking: %e s\n", end - start);
  }

  /* reduce integral value (mpi-reduce) */
  if (rank ==0) {
	  start = MPI_Wtime();
  }
  MPI_Reduce(&intg, &fintg, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (rank ==0) {
  	end = MPI_Wtime();
	  printf("Integration MPI_REDUCE reduction: %e s\n", end - start);
  }

  /* reduce integral value (manual tree-reduce) */
  fintg = intg;
  if (rank ==0) {
	  start = MPI_Wtime();
  }
  manual_tree_based_reduce(&fintg);
  if (rank ==0) {
  	end = MPI_Wtime();
	  printf("Integration P2P tree reduction: %e s\n", end - start);
  }

  /* calculate errors in dyc and intg */
  compute_errors(imin, imax, xc, dyc, derr);

  /* gather all derr/dyc data from all nodes */
  if (rank ==0) {
	  start = MPI_Wtime();
  }
  gather_from_all_procs(derr);
  if (rank ==0) {
  	end = MPI_Wtime();
	  printf("Error gather: %e s\n", end - start);
  }

  if (rank == 0) {
    compute_std_dev(derr, fintg, &davg_err, &dstd_dev, &intg_err);
    print_error_data(davg_err, dstd_dev, &xc[1], &derr[1], intg_err);
  }

  MPI_Finalize();

  /* free allocated memory */
  free(xc);
  free(yc);
  free(dyc);
  free(derr);

  return 0;
}
